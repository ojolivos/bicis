# Proyecto Bicicletas Urbanas
Primer entregable del curso Desarrollo del lado servidor: NodeJS, Express y MongoDB. Semana 1.

## Installation


```bash
npm install
```

## Usage



```bash
npm run devstart
```
[http://localhost:3000](http://localhost:3000)

## API


```bash
#Lista de Bicicletas
method: get
endpoint: http://localhost:3000/api/bicis/
```
```bash
#Ver bicicleta
method: get
endpoint: http://localhost:3000/api/bicis/:id/show
```
```bash
#Agregar Bicicleta
method: post
endpoint: http://localhost:3000/api/bicis/store
```
```bash
#Actualizar Bicicleta
method: post
endpoint: http://localhost:3000/api/bicis/:id/update
```

```bash
#Eliminar Bicicleta
method: get
endpoint: http://localhost:3000/api/bicis/:id/remove
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
