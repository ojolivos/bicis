const express  = require('express')
let router = express.Router()
const BicletasController = require('../controllers/api/BicicletaController')

router.get('/bicis', BicletasController.index)
router.get('/bicis/:id/show', BicletasController.show)
router.get('/bicis/:id/remove', BicletasController.remove)
router.post('/bicis/store', BicletasController.store)
router.post('/bicis/:id/update', BicletasController.update)

module.exports = router
