var Bicicleta = class Bicicleta {

    constructor(id = null, color = null, modelo = null, ubicacion = null) {
        this.id = id
        this.color = color
        this.modelo = modelo
        this.ubicacion = ubicacion

        this.allBicis = [
            {
                id: 1,
                color: 'Rojo',
                modelo: 'Marlin 6',
                ubicacion: ['5.3433241', '-72.3997452']
            },
            {
                id: 2,
                color: 'Verde',
                modelo: 'RockHopper',
                ubicacion: ['5.3453241', '-72.3947452']
            },
            {
                id: 3,
                color: 'Verde',
                modelo: 'RockHopper',
                ubicacion: ['5.3253241', '-72.3947452']
            }
        ]
    }

    store (bici) {
        this.allBicis.push(bici)
        return bici
    }

    remove (id) {
        this.allBicis.splice(this.allBicis.indexOf(this.allBicis.find((bici) => bici.id == id)),1)
        return {status: 'removed'}
    }
    show(id) {
        return this.allBicis.find(bici => bici.id == id)
    }

    update (bici, id) {
        let biciAnterior = this.allBicis.indexOf(this.allBicis.find(bici => bici.id == id))
        this.allBicis[biciAnterior] = bici
        return bici

    }

}

module.exports = Bicicleta
