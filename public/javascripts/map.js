
var mymap = L.map('mapid').setView([5.3333241, -72.3997452], 14);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(mymap);

fetch('api/bicis')
    .then(function(response) {
        return response.json();
    })
    .then(function(bicis) {
        bicis.forEach((bici) => {
            var marker = L.marker(bici.ubicacion,{title: `${bici.modelo} (${bici.color})` }).addTo(mymap);
        })
    });
