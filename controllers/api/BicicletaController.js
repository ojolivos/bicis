let Bicicleta = require('../../models/Bicicleta')
const Bici = new Bicicleta()

exports.index = (req, res) => {
    res.status(200).json(Bici.allBicis)
}

exports.show = (req, res) => {
    res.status(200).json(Bici.show(req.params.id))
}
exports.remove = (req, res) => {
    res.status(200).json(Bici.remove(req.params.id))
}

exports.store = (req, res) => {
    console.log(req)
    res.status(200).json(Bici.store({
        id: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.long]
    }))
}

exports.update = (req, res) => {
    res.status(200).json(Bici.update({
        id: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.long]
    },req.params.id))
}
